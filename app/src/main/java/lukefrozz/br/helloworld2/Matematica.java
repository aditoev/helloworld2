package lukefrozz.br.helloworld2;

/**
 * Classe de operações básicas
 * @author Luke F.
 * @since 14/09/2016
 * @version 1.0
 */
public class Matematica {
    /**
     * Método de soma
     * @param n1 primeiro parâmetro
     * @param n2 segundo parâmetro
     * @return soma dos dois parâmetros
     */
    public static Double somar(Double n1, Double n2) {
        return n1 + n2;
    }

    /**
     * Método de subtração
     * @param n1 primeiro parâmetro
     * @param n2 segundo parâmetro
     * @return subtração dos dois parâmetros
     */
    public static Double subtrair(Double n1, Double n2) {
        return n1 - n2;
    }

    /**
     * Método de multiplicação
     * @param n1 primeiro parâmetro
     * @param n2 segundo parâmetro
     * @return produto dos dois parâmetros
     */
    public static Double multiplicar(Double n1, Double n2) {
        return n1 * n2;
    }

    /**
     * Método de divisão
     * @param n1 primeiro parâmetro
     * @param n2 segundo parâmetro
     * @return divisão dos dois parâmetros
     */
    public static Double dividir(Double n1, Double n2) {
        return n1 / n2;
    }

}
