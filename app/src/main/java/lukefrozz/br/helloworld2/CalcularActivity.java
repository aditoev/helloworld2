package lukefrozz.br.helloworld2;

import android.renderscript.Double2;
import android.support.annotation.IntegerRes;
import android.support.annotation.StringDef;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class CalcularActivity extends AppCompatActivity {

    /**
     * Primeiro valor a receber da View
     */
    private EditText n1;
    /**
     * Segundo valor a receber da View
     */
    private EditText n2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calcular);

        this.n1 = (EditText) findViewById(R.id.calcEdN1);
        this.n2 = (EditText) findViewById(R.id.calcEdN2);
    }

    /**
     * Método para operar soma na Activity
     * @param v View da Activity
     */
    public void operarSoma(View v) {
        // Atribuição dos valores
        Double val1 = Double.parseDouble(this.n1.getText().toString());
        Double val2 = Double.parseDouble(this.n2.getText().toString());

        // Soma dos valores
        Double soma = Matematica.somar(val1, val2);

        // Envio da mensagem por Toast
        Toast.makeText(CalcularActivity.this, String.format("%.2f + %.2f = %.2f", val1, val2, soma), Toast.LENGTH_LONG).show();
    }

    /**
     * Método para operar subtração na Activity
     * @param v View da Activity
     */
    public void operarSubtracao(View v) {
        // Atribuição dos valores
        Double val1 = Double.parseDouble(this.n1.getText().toString());
        Double val2 = Double.parseDouble(this.n2.getText().toString());

        // Subtração dos valores
        Double subtracao = Matematica.subtrair(val1, val2);

        // Envio da mensagem por Toast
        Toast.makeText(CalcularActivity.this, String.format("%.2f - %.2f = %.2f", val1, val2, subtracao), Toast.LENGTH_LONG).show();
    }

    /**
     * Método para operar multiplicação na Activity
     * @param v View da Activity
     */
    public void operarMultiplicacao(View v) {
        // Atribuição dos valores
        Double val1 = Double.parseDouble(this.n1.getText().toString());
        Double val2 = Double.parseDouble(this.n2.getText().toString());

        // Multiplicação dos valores
        Double produto = Matematica.multiplicar(val1, val2);

        // Envio da mensagem por Toast
        Toast.makeText(CalcularActivity.this, String.format("%.2f * %.2f = %.2f", val1, val2, produto), Toast.LENGTH_LONG).show();
    }

    /**
     * Método para operar divisão na Activity
     * @param v View da Activity
     */
    public void operarDivisao(View v) {
        // Atribuição dos valores
        Double val1 = Double.parseDouble(this.n1.getText().toString());
        Double val2 = Double.parseDouble(this.n2.getText().toString());

        // Divisão dos valores
        Double divisao = Matematica.dividir(val1, val2);

        // Envio da mensagem por Toast
        Toast.makeText(CalcularActivity.this, String.format("%.2f : %2.f = %s", val1, val2, divisao), Toast.LENGTH_LONG).show();
    }

}
