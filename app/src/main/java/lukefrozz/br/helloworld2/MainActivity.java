package lukefrozz.br.helloworld2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText user;
    private EditText pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.user = (EditText) findViewById(R.id.loginEdUser);
        this.pass = (EditText) findViewById(R.id.loginEdSenha);
    }

    public void login(View v) {
        String usr, senha;

        usr = this.user.getText().toString();
        senha = this.pass.getText().toString();

        if (usr.equalsIgnoreCase("lukefrozz") &&
            senha.equalsIgnoreCase("123456")) {
            Toast.makeText(MainActivity.this, "Usuário Autenticado", Toast.LENGTH_SHORT).show();

            Intent i = new Intent(this, CalcularActivity.class);
            startActivity(i);
        } else {
            Toast.makeText(MainActivity.this, "Usuário, e-mail e/ou senha incorretos", Toast.LENGTH_LONG).show();
        }
    }
}
