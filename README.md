# Segunda aula de Android #

##Considerações##

* A API do Android configurada nesse projeto é a API 19
* Os commits são dados em meio a um contexto segundo a aula
* O projeto é compilado sem erros de acordo com a máquina utilizada pelo @lukefrozz em sala de aula, desenvolvedor proprietário do repositório.

##Como clonar esse repositório##

1. Copie o link na parte superior Direita da tela que se encontra ao lado de **HTTPS**
2. Abra o **Android Studio** e na tela inicial (a que vem antes de abrir ou criar um projeto) clique em *Check out project from Version Control* e selecione a opção **Git**, mas atenção, não é a opção **GitHub**
3. Cole o link do *Passo 1* no primeiro campo e clique em *Test* para saber se o Android Studio está reconhecendo o repositório
4. O segundo campo é para escolher onde ficará localizado o projeto, por padrão, ele fica na pasta **AndroidStudioProjects**
5. No terceiro campo, vai o nome do projeto, pode alterá-lo, a recomendação é que troque a *primeira letra* do nome escrito pela mesma maiúscula.
6. Clique em **clone** e aguarde a IDE carregar o projeto.

###That's All Folks###

####Observações####

Caso sua IDE esteja com um projeto aberto basta seguir esse passo-a-passo:

* Clique em **File** no canto superior esquedo da tela
* Clique em **Close project**, na 4º opção, com isso sua IDE volta para a tela inicial